<?php include('header.php'); ?>
<?php include('connect.php'); ?>

</div>

<div class="container list">
	<form method="post" enctype="multipart/form-data" name="formUploadFile">
		<div class="form-group">
		    <label for="category">Category:</label>
		    <input type="text" name="category" class="form-control categ" id="cat">
		</div>		
		<label>Select files (max 20) to upload:</label>
		<input type="file" name="files[]" multiple="multiple" class="choose" />
		<input type="submit" value="Upload File" name="btnSubmit" class="upload admin-btn"/>
	</form>		
		<?php
			if(isset($_POST["btnSubmit"]))
			{
				$errors = array();
				$uploadedFiles = array();
				$extension = array("jpeg","jpg","png","gif");
				$bytes = 1024;
				$KB = 1024;
				$totalBytes = 10 * $bytes * $KB;
				$UploadFolder = "UploadFolder";
				$category = $_POST['category'];
				
				
				$counter = 0;
				
				foreach($_FILES["files"]["tmp_name"] as $key=>$tmp_name){
					$temp = $_FILES["files"]["tmp_name"][$key];
					$name = $_FILES["files"]["name"][$key];
					if(empty($temp))
					{
						break;
					}
					
					$counter++;
					$UploadOk = true;
					
					if($_FILES["files"]["size"][$key] > $totalBytes)
					{
						$UploadOk = false;
						array_push($errors, $name." file size is too large.");
					}
					
					$ext = pathinfo($name, PATHINFO_EXTENSION);
					if(in_array($ext, $extension) == false){
						$UploadOk = false;
						array_push($errors, $name." is invalid file type.");
					}
					
					if(file_exists($UploadFolder."/".$name) == true){
						$UploadOk = false;
						array_push($errors, $name." file is already exist.");
					}
					
					if($UploadOk == true){
						move_uploaded_file($temp,$UploadFolder."/".$name);
						array_push($uploadedFiles, $name);

						$sql = "INSERT INTO `photos` (`category`, `name`) VALUES ('$category', '$name')";
						if ($conn->query($sql) === TRUE) {
						  //echo '<br/> Datele au fost adaugate';
						}
						else {
						 //echo 'Error: '. $conn->error;
						}
					}

					

				}
				
				if($counter>0){
					if(count($errors)>0)
					{
						echo "<b>Errors:</b>";
						echo "<br/><ul>";
						foreach($errors as $error)
						{
							echo "<li>".$error."</li>";
						}
						echo "</ul><br/>";
					}
					
					if(count($uploadedFiles)>0){
						echo "<b>Uploaded Files:</b>";
						echo "<br/><ul>";
						foreach($uploadedFiles as $fileName)
						{
							echo "<li>".$fileName."</li>";
						}
						echo "</ul><br/>";
						
						echo count($uploadedFiles)." file(s) are successfully uploaded.";
					}								
				}
				else{
					echo "Please, Select file(s) to upload.";
				}
				
			}
		?>


</div> <!-- end container -->

<?php include('footer.php'); ?>
