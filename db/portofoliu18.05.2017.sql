-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 18 Mai 2017 la 17:17
-- Versiune server: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `portofoliu`
--

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Salvarea datelor din tabel `categories`
--

INSERT INTO `categories` (`id`, `name`) VALUES
(1, 'Category1'),
(2, 'Category2'),
(3, 'Category3'),
(4, 'Category4');

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `photos`
--

CREATE TABLE `photos` (
  `id` int(10) NOT NULL,
  `name` varchar(255) NOT NULL,
  `category` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Salvarea datelor din tabel `photos`
--

INSERT INTO `photos` (`id`, `name`, `category`) VALUES
(25, '1.jpg', 'Category1'),
(26, '2.jpg', 'Category1'),
(27, '3.jpg', 'Category1'),
(28, '7.jpg', 'Category3'),
(29, '8.jpg', 'Category3'),
(30, '9.jpg', 'Category3'),
(31, '4.jpg', 'Category2'),
(32, '5.jpg', 'Category2'),
(33, '6.jpg', 'Category2'),
(34, 'photo1.jpg', 'Category4'),
(36, '8 - Copy.jpg', 'Category1'),
(37, '9 - Copy.jpg', 'Category1'),
(38, 'fotografie1 - Copy.jpg', 'Category3'),
(39, 'fotografie2 - Copy.jpg', 'Category3'),
(40, 'fotografie3 - Copy.jpg', 'Category3'),
(41, 'fotografie1.jpg', 'Category4'),
(43, 'fotografie3.jpg', 'Category4'),
(44, 'fotografie4.jpg', 'Category3'),
(45, 'fotografie5.jpg', 'Category3'),
(46, 'fotografie6.jpg', 'Category3'),
(47, 'photo1.jpg', ''),
(48, 'photo1.jpg', 'Category1'),
(49, 'photo2.jpg', 'Category1'),
(50, 'photo3.jpg', 'Category1'),
(51, '1 - Copy.jpg', 'Category1'),
(52, '1.jpg', 'Category1'),
(53, '2 - Copy.jpg', 'Category1'),
(54, '2.jpg', 'Category1'),
(55, '3 - Copy.jpg', 'Category1'),
(56, '3.jpg', 'Category1'),
(57, '4 - Copy.jpg', 'Category1'),
(58, '4.jpg', 'Category1'),
(59, '5 - Copy.jpg', 'Category1'),
(60, '5.jpg', 'Category1'),
(61, '6 - Copy.jpg', 'Category1'),
(62, '6.jpg', 'Category1'),
(63, '7 - Copy - Copy.jpg', 'Category1'),
(64, '7 - Copy (2).jpg', 'Category1'),
(65, '7 - Copy.jpg', 'Category1'),
(66, '7.jpg', 'Category1'),
(67, '8 - Copy - Copy.jpg', 'Category1'),
(68, '8 - Copy (2).jpg', 'Category1'),
(69, '8 - Copy.jpg', 'Category1'),
(70, '8.jpg', 'Category1'),
(71, '1 - Copy.jpg', ''),
(72, '1.jpg', ''),
(73, '2 - Copy.jpg', ''),
(74, '2.jpg', ''),
(75, '3 - Copy.jpg', ''),
(76, '3.jpg', ''),
(77, '4 - Copy.jpg', ''),
(78, '4.jpg', ''),
(79, '5 - Copy.jpg', ''),
(80, '5.jpg', ''),
(81, '6 - Copy.jpg', ''),
(82, '6.jpg', ''),
(83, '7 - Copy - Copy.jpg', ''),
(84, '7 - Copy (2).jpg', ''),
(85, '7 - Copy.jpg', ''),
(86, '7.jpg', ''),
(87, '8 - Copy - Copy.jpg', ''),
(88, '8 - Copy (2).jpg', ''),
(89, '8 - Copy.jpg', ''),
(90, '8.jpg', ''),
(91, '9 - Copy - Copy.jpg', ''),
(92, '9 - Copy (2).jpg', ''),
(93, '9 - Copy.jpg', ''),
(94, '9.jpg', ''),
(95, 'fotografie1 - Copy - Copy.jpg', ''),
(96, 'fotografie1 - Copy (2).jpg', ''),
(97, 'fotografie1 - Copy.jpg', ''),
(98, 'fotografie1.jpg', ''),
(99, 'fotografie2 - Copy.jpg', ''),
(100, 'fotografie2.jpg', ''),
(101, 'fotografie3 - Copy.jpg', ''),
(102, 'fotografie3.jpg', ''),
(103, 'fotografie4.jpg', ''),
(104, 'fotografie5.jpg', ''),
(105, 'fotografie6.jpg', ''),
(106, 'photo1.jpg', ''),
(107, 'photo2.jpg', ''),
(108, 'photo3.jpg', ''),
(109, 'fotografie2 - Copy.jpg', 'Category2'),
(110, 'fotografie2.jpg', 'Category2'),
(111, 'fotografie3 - Copy - Copy.jpg', 'Category2'),
(112, 'fotografie3 - Copy (2).jpg', 'Category2'),
(113, 'fotografie3 - Copy.jpg', 'Category2'),
(114, 'fotografie3.jpg', 'Category2'),
(115, 'fotografie4.jpg', 'Category2'),
(116, 'fotografie5.jpg', 'Category2'),
(117, 'fotografie6.jpg', 'Category2'),
(118, 'photo1 - Copy.jpg', 'Category2'),
(119, 'photo1.jpg', 'Category2'),
(120, 'photo2 - Copy.jpg', 'Category2'),
(121, 'photo2.jpg', 'Category2'),
(122, 'photo3 - Copy.jpg', 'Category2'),
(123, 'photo3.jpg', 'Category2'),
(124, '7 - Copy - Copy.jpg', ''),
(125, '7 - Copy (2).jpg', ''),
(126, '7 - Copy.jpg', ''),
(127, '7.jpg', ''),
(128, '8 - Copy - Copy.jpg', ''),
(129, '8 - Copy (2).jpg', ''),
(130, '8 - Copy.jpg', ''),
(131, '8.jpg', ''),
(132, '9 - Copy - Copy.jpg', ''),
(133, '9 - Copy (2).jpg', ''),
(134, '9 - Copy.jpg', ''),
(135, '9.jpg', ''),
(136, 'fotografie1 - Copy - Copy.jpg', ''),
(137, 'fotografie1 - Copy (2).jpg', ''),
(138, 'fotografie1 - Copy.jpg', ''),
(139, 'fotografie1.jpg', ''),
(140, 'fotografie2 - Copy - Copy.jpg', ''),
(141, 'fotografie2 - Copy (2).jpg', '');

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `users`
--

CREATE TABLE `users` (
  `id` int(10) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Salvarea datelor din tabel `users`
--

INSERT INTO `users` (`id`, `username`, `password`) VALUES
(1, 'admin', 'photoadmin');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `photos`
--
ALTER TABLE `photos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `photos`
--
ALTER TABLE `photos`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=142;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
