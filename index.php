<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->

<?php session_start(); ?>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Masonry Responsive Template</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/font-awesome.css">

        
        <!-- <link rel="stylesheet" href="css/bootstrap.min.css"> -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous"> 
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.1.1/ekko-lightbox.css">
        <link rel="stylesheet" href="css/templatemo-style.css">
        <script src="js/vendor/modernizr-2.6.2.min.js"></script>
    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        
        <div id="loader-wrapper">
            <div id="loader"></div>
        </div>

        <div class="content-bg"></div>
        <div class="bg-overlay"></div>

        <!-- SITE TOP -->
        <div class="site-top">
            <div class="site-header clearfix">
                <div class="container">
                    <a href="#" class="site-brand pull-left"><strong>Masonry</strong> Free Template</a>
                    <div class="social-icons pull-right">
                        <ul>
                            <li><a href="#" class="fa fa-facebook"></a></li>
                            <li><a href="#" class="fa fa-twitter"></a></li>
                            <li><a href="#" class="fa fa-behance"></a></li>
                            <li><a href="#" class="fa fa-dribbble"></a></li>
                            <li><a href="#" class="fa fa-google-plus"></a></li>
                            <?php if (isset($_SESSION['username'])) { ?>
                                <li class="admin-btn"><a href="list.php">LIST</a></li>
                                <li class="admin-btn"><a href="add.php">ADD</a></li>
                                <li class="admin-btn"><a href="logout.php">LOGOUT</a></li>
                            <?php }else{ ?>
                                <li class="admin-btn"><a href="login.php">LOGIN</a></li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            </div> <!-- .site-header -->

            <?php include('connect.php'); ?>

            <!-- carousel start -->
            <div id="myCarousel" class="carousel slide" data-ride="carousel">
			    <!-- Indicators -->
			    <ol class="carousel-indicators">
			      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
			      <li data-target="#myCarousel" data-slide-to="1"></li>
			      <li data-target="#myCarousel" data-slide-to="2"></li>
			    </ol>

			    <!-- Wrapper for slides -->
			    <div class="carousel-inner">
			      <div class="item active" style="background-image: url('images/photo2.jpg');">
			        <!-- <img src="images/photo2.jpg" alt="Los Angeles" style="width:100%;"> -->
			      </div>

			      <div class="item" style="background-image: url('images/photo1.jpg');">
			       <!--  <img src="images/photo1.jpg" alt="Chicago" style="width:100%;"> -->
			      </div>
			    
			      <div class="item" style="background-image: url('images/photo3.jpg');">
			        <!-- <img src="images/photo3.jpg" alt="New york" style="width:100%;"> -->
			      </div>
			    </div>

			    <!-- Left and right controls -->
			    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
			      <span class="glyphicon glyphicon-chevron-left"></span>
			      <span class="sr-only">Previous</span>
			    </a>
			    <a class="right carousel-control" href="#myCarousel" data-slide="next">
			      <span class="glyphicon glyphicon-chevron-right"></span>
			      <span class="sr-only">Next</span>
			    </a>
			  </div> <!-- end carousel -->

        </div> <!-- .site-top -->
        
	
        <?php


			$sql = "SELECT `id`, `name` FROM `categories`";
			$result = $conn->query($sql);
            ?>

        <!-- MAIN POSTS -->
        <div class="main-posts">
        	<div class="container">
                <?php
                if ($result->num_rows > 0) { ?>
                      <ul class="categories">
                <?php
                  while($row = $result->fetch_assoc()) {
                    ?>
                    <li><?php echo $row['name'] ?></li>                
                    <?php
                  }
                  ?>
                  </ul> 
                   <?php
            }
            else {
              echo '0 rezultate';
            }
            ?>
        	</div>

            <?php
            $result2 = $conn->query($sql);
            if ($result2->num_rows > 0) {
                while($row = $result2->fetch_assoc()) {
                    $category = $row['name'];
            ?>

                <div class="container cat <?php echo $category; ?>">
                <div class="row">
                    <div class="blog-masonry masonry-true">  
                    
                    <?php 
                        $sql1 = "SELECT `id`, `name`, `category` FROM `photos` WHERE `category`='$category' "; 
                        $result1 = $conn->query($sql1);
                        if ($result1->num_rows > 0) {
                            while($row = $result1->fetch_assoc()) { ?>
                                <?php $image = $row['name']; ?>
                                    <div class="post-masonry col-md-4 col-sm-6">
                                        <div class="post-thumb">
                                        <?php $link_img = 'UploadFolder/'.$image; ?>
                                            <a href="<?php echo $link_img; ?>" data-toggle="lightbox" data-gallery="example-gallery">
                                                <img src="<?php echo $link_img; ?>" alt=" ">
                                            </a>
                                        </div>
                                    </div> <!-- /.post-masonry -->
                            <?php }
                        }
                        else {
                          echo '0 rezultate';
                        }
                        ?>


                    </div>
                </div>
            </div>
            
            <?php } //end while?>
            <?php } //end if
            else {
              echo '0 rezultate';
            }
            ?>
           
        </div>

<?php include('footer.php'); ?>
        