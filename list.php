<?php include('header.php'); ?>
<?php include('connect.php'); ?>

</div>

<div class="container list">

<div class='tablehead'>
	<ul>
		<li class="col-lg-3">Preview</li>
		<li class="col-lg-4">Name</li>
		<li class="col-lg-3">Category</li>
		<li class="col-lg-2">Delete</li>
	</ul>
</div>
<?php
	$sql = "SELECT `id`, `name` FROM `categories`"; 
	$result = $conn->query($sql);

	if ($result->num_rows > 0) {
	    while($row = $result->fetch_assoc()) { ?>
		    <?php $category = $row['name']; ?>
			<div class="photos<?php echo $category; ?>">
				<?php
					$sql1 = "SELECT `id`, `name`, `category` FROM `photos` WHERE `category`='$category' "; 
					$result1 = $conn->query($sql1);
					if ($result1->num_rows > 0) {
	    				while($row = $result1->fetch_assoc()) { ?>
	    					<?php $image = $row['name']; ?>
	    					<div class="singlePhoto">
								<div class="col-lg-3">
									<?php $link_img = 'UploadFolder/'.$image; ?>
									<img src="<?php echo $link_img; ?>" alt=""><br>
								</div>
								<div class="col-lg-4">
									<?php echo $image; ?>
								</div>
								<div class="col-lg-3">
									<?php echo $category; ?>
								</div>
								<div class="col-lg-2">
									<a class="delete-btn" href="delete.php?id=<?php echo $row['id'] ?>">
									<span class="fa fa-close"></span>
									</a>
								</div>
							</div>

						<?php }
					}
					else {
					  echo '0 rezultate';
					}
					?>
			</div>
	<?php }
	}
	else {
	  echo '0 rezultate';
	}
	?>

</div> <!-- end container -->

<?php include('footer.php'); ?>
