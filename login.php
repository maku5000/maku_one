<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Masonry Responsive Template</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/font-awesome.css">
        <!-- <link rel="stylesheet" href="css/bootstrap.min.css"> -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
		<!-- Optional theme -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
        <link rel="stylesheet" href="css/templatemo-style.css">
        <script src="js/vendor/modernizr-2.6.2.min.js"></script>
    </head>
    <body>
        <div class="content-bg"></div>
        <div class="site-top">
            <div class="site-header clearfix">
                <div class="container">
                    <a href="#" class="site-brand pull-left"><strong>Masonry</strong> Free Template</a>
                    
                </div>
            </div> <!-- .site-header -->
</div>
<?php include('connect.php'); ?>
<?php session_start(); ?>
</div>

<div class="container form">
    <form class="form-login" name="form-login" method="post" action="#">
      <input name="username" id="username" type="text" class="form-control" placeholder="Username" autofocus>
      <input name="password" id="password" type="password" class="form-control" placeholder="Password">
      <button name="Submit" id="submit" class="btn btn-lg btn-primary btn-block" type="submit">Login</button>
    </form>
</div>

 <?php 

    if (isset($_POST['Submit'])) {  
      $username = mysqli_real_escape_string($conn, $_POST['username']);
      $password = mysqli_real_escape_string($conn, $_POST['password']);   
      
      $sql = "SELECT * FROM users WHERE `username` = '$username' AND `password` = '$password'";
      $result = $conn->query($sql);
      // daca $result contine cel putin un rand
      if ($result->num_rows > 0) {
          $_SESSION['username'] = $username;
          $_SESSION['loggedin'] = true;
          //header("location: categories.php");
          ?> 
          <script type="text/javascript">
            window.location.href = 'index.php';
          </script>
          <?php
      }else{
        echo '<div id="error-login">The username or password is incorect.</div>';
      }
    }

    ?>

<?php include('footer.php'); ?>